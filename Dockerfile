FROM php:7.3

# Update and Install some packages
RUN apt-get --quiet update --yes && \
    apt-get --quiet install --yes wget openssh-server nodejs rsync zip unzip git -y

# Install composer and terminus
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN composer self-update
RUN mkdir -p /usr/local/share/terminus
RUN /usr/bin/env COMPOSER_BIN_DIR=/usr/local/bin composer -n --working-dir=/usr/local/share/terminus require pantheon-systems/terminus:"^2"
